Source: unicap
Section: libs
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-buildinfo,
 gnulib,
 gtk-doc-tools,
 intltool,
 libglib2.0-dev,
 libraw1394-dev,
 libtool,
 libv4l-dev,
 pkgconf,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/multimedia-team/unicap.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/unicap
Homepage: https://unicap-imaging.org/

Package: libunicap2-dev
Section: libdevel
Depends:
 libunicap2 (= ${binary:Version}),
 ${misc:Depends},
Provides:
 libunicap-dev,
Conflicts:
 libunicap-dev,
Suggests:
 libunicap-doc,
Architecture: any
Multi-Arch: same
Description: unified interface to video capture devices - development files
 Unicap provides a uniform interface to video capture devices. It allows
 applications to use any supported video capture device via a single
 API.
 .
 Unicap offers a high level of hardware abstraction while maintaining
 maximum performance. Zero copy capture of video buffers is possible for
 devices supporting it allowing fast video capture with low CPU usage
 even on low-speed architectures.
 .
 This package contains the development headers and static libraries.

Package: libunicap2
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Architecture: any
Multi-Arch: same
Description: unified interface to video capture devices - shared libraries
 Unicap provides a uniform interface to video capture devices. It allows
 applications to use any supported video capture device via a single
 API.
 .
 Unicap offers a high level of hardware abstraction while maintaining
 maximum performance. Zero copy capture of video buffers is possible for
 devices supporting it allowing fast video capture with low CPU usage
 even on low-speed architectures.
 .
 This package contains the shared libraries.

Package: libunicap-doc
Section: doc
Depends:
 ${misc:Depends},
Provides:
 libunicap-docs,
Breaks:
 libunicap-docs,
Replaces:
 libunicap-docs,
Architecture: all
Multi-Arch: foreign
Description: unified interface to video capture devices - documentation
 Unicap provides a uniform interface to video capture devices. It allows
 applications to use any supported video capture device via a single
 API.
 .
 Unicap offers a high level of hardware abstraction while maintaining
 maximum performance. Zero copy capture of video buffers is possible for
 devices supporting it allowing fast video capture with low CPU usage
 even on low-speed architectures.
 .
 This package contains documentation.
